package ru.tsc.almukhametov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.entity.IHasFinishDate;

import java.util.Comparator;

public class ComparatorByFinishDate implements Comparator<IHasFinishDate> {

    @NotNull
    private static final ComparatorByFinishDate INSTANCE = new ComparatorByFinishDate();

    private ComparatorByFinishDate() {
    }

    @NotNull
    public static ComparatorByFinishDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable IHasFinishDate o1, @Nullable IHasFinishDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getFinishDate().compareTo(o2.getFinishDate());
    }
}

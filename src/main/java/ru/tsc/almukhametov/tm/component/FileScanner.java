package ru.tsc.almukhametov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final String path = "./";

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getArguments()) {
            commands.addAll(bootstrap.getCommandService().getListCommandName());
        }
        final Long lag = bootstrap.getPropertyService().getFileScannerInterval();
        executorService.scheduleWithFixedDelay(this, lag, lag, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull File file = new File(path);
        for (File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String filename = item.getName();
            if (!commands.contains(filename)) continue;
            try {
                bootstrap.parseCommands(filename);
            } catch (Exception e) {
                bootstrap.getLogService().info("Info file scanner: " + e.getMessage());
            }
            item.delete();
        }
    }
}
